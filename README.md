**Code of - "Clustering Crowdsourced Test Reports of Mobile Applications Using Image Understanding"**

**A simple illustration:**

**SPM(in Matlab):** contains Spatial Pyramid Matching(SPM), scale-invariant feature transform(SIFT) descriptors extraction, histograms building, and histogram distance calculation for screenshots

**stopwords1893.txt:** the stopwords list

 **feature_extractor.py:** the matrix building (with different features)

**model.py & read_data.py:** data reading and report object generation 

**clustering.py:** clustering and cluster index building
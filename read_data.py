# -*- coding: UTF-8 -*-
from model import *
import xlrd
import jieba


def reform_float_to_str(float_num):
    str_num = str(float_num)
    str_num = str_num.replace('.0', '')
    return str_num


def reform_description(description):
    description = description.strip()
    description = description.replace('\n', '')
    return description


def reform_img_url(img_url):
    img_url = img_url.replace(' ', '')
    img_url = img_url.replace(';', '\n')
    img_url = img_url.split('\n')
    return img_url


def extract_keyword(report):
    f_stopwords = open('stopwords1893.txt', encoding='utf-8')
    stopwords = f_stopwords.read()
    stopwords = stopwords.split('\n')

    report.keyword = jieba.lcut(report.description, cut_all=False)
    keyword_1 = []
    for word in report.keyword:  # filter stopwords
        if word not in stopwords:
            keyword_1.append(word)
    report.keyword = keyword_1


def load_caselist(project):
    ExcelPath = 'E:/data/report/' + project + '.xls'
    workbook = xlrd.open_workbook(ExcelPath)
    sheet1 = workbook.sheet_by_name(project)
    nrows = sheet1.nrows

    caselist = []

    bug_id_col_index = 0
    report_id_col_index = 1
    bug_category_col_index = 10
    description_col_index = 4
    img_url_col_index = 5
    severity_col_index = 6
    recurrent_col_index = 7

    case_id = 0
    for row in range(1, nrows):
        newcase = case(case_id)
        newcase.bug_id = reform_float_to_str(sheet1.cell_value(row, bug_id_col_index))
        newcase.report_id = reform_float_to_str(sheet1.cell_value(row, report_id_col_index))
        newcase.bug_category = sheet1.cell_value(row, bug_category_col_index)
        newcase.description = reform_description(sheet1.cell_value(row, description_col_index))
        newcase.shotlist = sheet1.cell_value(row, img_url_col_index)
        newcase.severity = reform_float_to_str(sheet1.cell_value(row, severity_col_index))
        newcase.recurrent = reform_float_to_str(sheet1.cell_value(row, recurrent_col_index))

        if newcase.shotlist.strip() != '':  # filtering reports without images
            newcase.shotlist = reform_img_url(newcase.shotlist)
            caselist.append(newcase)
            case_id += 1

    for report in caselist:
        extract_keyword(report)

    return caselist


def main():
    project = 'MyListening'
    caselist = load_caselist(project)
    for case in caselist:
        case.print_case()

if __name__ == '__main__':
    main()
